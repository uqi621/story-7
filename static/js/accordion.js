$( function() {
	$( "#accordion" ).accordion({
		collapsible: true
	});
});

$(document).ready(function () {
    var color = 0;
    $(".change").click(function(){

    	if (color === 0){
    		$(".frontPage").css("filter", "grayscale(0)");
    		$(".acc_bw").toggleClass('acc_rgb');
    		$(".acc_bw").toggleClass('acc_bw');
    		$(".bw").toggleClass('rgb');
    		$(".bw").toggleClass('bw');
    	}
    	else{
    		$(".frontPage").css("filter", "grayscale(100)");
    		$(".acc_rgb").toggleClass('acc_bw');
    		$(".acc_rgb").toggleClass('acc_rgb');
    		$(".rgb").toggleClass('bw');
    		$(".rgb").toggleClass('rgb');
    	}
    color = (color + 1) % 2;
    });
});
