from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from django.apps import apps
from .apps import Apps7Config

# Create your tests here.

class Story7Test(TestCase):
	def test_apps(self):
		self.assertEqual(Apps7Config.name, 'appS7')
		self.assertEqual(apps.get_app_config('appS7').name, 'appS7')
	def test_url(self):
		response = Client().get('/about')
		self.assertNotEqual(response.status_code, 200)
		response = Client().get('/books')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/home')
		self.assertNotEqual(response.status_code, 200)
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)